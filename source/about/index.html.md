---
layout: markdown_page
title: About Us
---

1. GitLab is a [single application](/handbook/product/single-application/) with [features](/features/) for the whole software development and operations (DevOps) [lifecycle](/sdlc/).
1. GitLab, the open source project, is used by more than [100,000 organizations](/is-it-any-good/) and has a large [community](/community/) of more than [2000 people who contributed code](http://contributors.gitlab.com/).
1. GitLab Inc. is a open-core company that sells [subscriptions that offer more feature and support for GitLab](/products).
1. The company is [remote only](/culture/remote-only/) with [300+ people working from their own location in more than 39 countries](/team/).
1. GitLab Inc. is an active participant in this community, see [our stewardship](/stewardship) for more information.
1. [Our history](/history) starts in 2011 for the open source project, and in 2015 we joined Y Combinator and started growing faster, we have a public [strategy](/strategy/#sequence).
1. [Our mission](/strategy/#mission) is to change all creative work from read-only to read-write so that **everyone can contribute**.
1. Our Tanuki (Japanese for raccoon dog) logo symbolizes this with a smart animal that works in a group to achieve a common goal, you can download it on our [press page](/press).
1. [Our values](/handbook/values) are Collaboration, Results, Efficiency, Diversity, Iteration, and Transparency (CREDIT) and these form an important part of our [culture](/culture).
1. Most of our internal procedures can be found in [publicly viewable 1000+ page handbook](/handbook) and our objects are documented in our [OKRs](/okrs/).
1. We [release](/releases/) every month on the 22nd and there is a [publicly viewable direction for the product](/direction/#scope).
