---
layout: job_page
title: "Chief Executive Officer"
---

The CEO reports to the board of directors of GitLab, Inc.

## Responsibilities

* Set overall strategy.
* Hire great people. Help people that are not a good fit find another job.
* Provide direction for product development with the [CTO](/jobs/chief-technology-officer)
  and the [Product Manager](/job-families/product/product-manager/).
* Communicate company vision and goals to the entire team and the rest of the community.
* Lead by example.
* Keep the board of directors and investors well-informed and engaged in GitLab's continued growth and success.
* Ensure there is enough cash at all times. Secure funding for the company if needed.
* Continue to be deeply engaged with the entire GitLab community through active discussions on Hacker News, Twitter, etc.
* Make sure that GitLab Inc. acts in the interest of the GitLab community.
* Pricing and business model.
* Actively engage in mergers and acquisitions if and when those are appropriate.
* Oversee the continued build up of a great team.
* Must successfully complete a [background check](/handbook/people-operations/code-of-conduct/#background-checks).
