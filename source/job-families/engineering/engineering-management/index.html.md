---
layout: job_page
title: "Engineering Management"
---

## Engineering Management Roles at GitLab

Managers in the engineering department at GitLab see the team as their product. While they are technically credible and know the details of what developers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of product commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### Engineering Manager

Unless otherwise specified below, all Engineering Manager roles at GitLab share
the following requirements and responsibilities:

#### Responsibilities

* Help your developers grow their skills and experience
* Author project plans for epics
* Run agile project management processes
* Conduct code reviews, and make technical contributions to product
  architecture as well as getting involved in solving bugs and delivering small
  features
* Actively seek and hire globally-distributed talent
* Conduct managerial interviews for candidates, and train the team to screen candidates
* Contribute to the sense of psychological safety on your team
* Generate and implement process improvements
* Hold regular 1:1's with all members their team
* Foster technical decision making on the team, but make final decisions when necessary
* Author project plans for epics
* Draft quarterly OKRs
* Train engineers to screen candidates and conduct managerial interviews
* Improve product quality, security, and performance

#### Requirements

* Exquisite communication: Regularly achieve consensus amongst departments
* 5 years or more experience in a leadership role with current technical experience
* In-depth experience with Ruby on Rails, Go, and/or Git, in addition to any
  experience required by the position's [specialty](#specialties)
* Excellent written and verbal communication skills
* You share our [values](/handbook/values), and work in accordance with those values

#### Nice-to-have's

* Experience in a peak performance organization
* Deep Ruby on Rails experience
* Product company experience
* Startup experience
* Enterprise software company experience
* Computer science education or equivalent experience
* Passionate about open source and developer tools

#### Hiring Process

Candidates for this position can generally expect the hiring process to follow the order below. Note that as candidates indicate preference or aptitude for one or more [specialties](#specialties), the hiring process will be adjusted to suit. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Recruiting team
* Next, candidates will be invited to schedule a 60 minute first interview with a Director of Engineering, Backend
* Next, candidates will be invited to schedule a 45 minute second peer interview with an Engineering Manager
* Next, candidates will be invited to schedule a 45 minute third interview with one or more members of the Engineering team
* Next, candidates will be invited to schedule a 45 minute fourth interview with our VP of Engineering
* Finally, candidates may be asked to schedule a 50 minute final interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

### Director of Engineering

The Director of Engineering role extends the [Engineering Manager](#engineering-manager) role.

* Hire a world class team of managers and developers to work on their teams
* Help their managers and developers grow their skills and experience
* Manage multiple teams and projects
* Hold regular skip-level 1:1's with all members of their team
* Create a sense of psychological safety on your _teams_
* _Drive_ technical and process improvements
* _Drive_ quarterly OKRs
* _Drive_ agile project management process
* _Own_ product quality, security, and performance
* Represent the company publicly at conferences

The Director of Engineering, Backend at GitLab manages multiple backend teams in the product development organization. They see their teams as their product. They are capable of managing multiple teams and projects at the same time. They are expert recruiters of both developers and managers alike. But they can also grow the existing talent on their teams. Directors are leaders that model the behaviors we want to see in our teams and hold others accountable when necessary. And they create the collaborative and productive environment in which developers and engineering managers do their work.

#### Responsibilities

* Hire and manage multiple backend teams that lives our [values](/handbook/values/)
* Measure and improve the happiness and productivity of the team
* Manage the agile development process
* Drive quarterly OKRs
* Work across sub-departments within engineering
* Own the quality, security and performance of the product
* Write public blog posts and speak at conferences

#### Requirements

* 10 years managing multiple software engineering teams
* Experience in a peak performance organization
* Deep Ruby on Rails experience
* Product company experience
* Startup experience
* Enterprise software company experience
* Computer science education or equivalent experience
* Passionate about open source and developer tools
* Exquisite communication skills

#### Nice-to-have's

* Kubernetes, Docker, Go, Helm, or Linux administration
* Online community participation
* Remote work experience
* Significant open source contributions

### VP of Engineering

The VP of Engineering role extends the [Director of Engineering](#director-of-engineering) role.

* Drive recruiting of a world class team
* Help their directors, managers, and developers grow their skills and experience
* Measure and improve the happiness of engineering
* Make sure the handbook is used and maintained in a transparent way
* _Sponsor_ technical and process improvements
* _Own_ the sense of psychological safety of the department
* _Set_ quarterly OKRs around company goals
* _Define_ the agile project management process
* Spend time with customers to understand their needs and issues
* _Be accountable for_ product quality, security, and performance

## Specialties

### Auth

The Engineering Manager, Auth directly manages the developers working on
features and functionality related to the Auth product category, as described on
our [product
categories](/handbook/product/categories/) page.

See [Engineering Manager](#engineering-manager) for more details.

#### Nice-to-have's

* Experience with SSO integrations (e.g. LDAP)
* Familiarity with internationalization best practices
* Experience with SaaS billing and subscription systems

#### Hiring Process

The hiring process for this specialty follows the process for most [Engineering
Managers](#engineering-manager), except for the following:

* The peer interview for any managers considered for the Auth team will be
conducted by our Engineering Manager, Platform.

### CI/CD

The Engineering Manager, CI/CD, directly manages the developers that develop
CI/CD features for GitLab.

See [Engineering Manager](#engineering-manager) for more details.

### Geo

[GitLab Geo](https://docs.gitlab.com/ee/gitlab-geo/README.html) is an
enterprise product feature set that speeds up the work of globally distributed
teams, adds redundancy for GitLab instances, and provides Disaster Recovery as
well. As the Engineering Manager for the Geo team, you will take overall responsibility for
hiring, team management, agile project management, and the quality of the
feature set. This position reports to the Director of Engineering, Dev Backend.

See [Engineering Manager](#engineering-manager) for more details.

#### Responsibilities

In addition to the general responsibilities of the Engineering Manager role, the
Geo team specifically requires you to be involved in and responsible for:
- Architecting Geo and Disaster Recovery products for GitLab
- Identifying ways to test and improve availability and performance of GitLab Geo at GitLab.com scale
- Instrument and monitor the health of distributed GitLab instances
- Educate all team members on best practices relating to high availability

#### Requirements

- Experience architecting and implementing fault-tolerant, distributed systems


### Monitoring

[GitLab Monitoring](https://docs.gitlab.com/ee/administration/monitoring/prometheus/gitlab_metrics.html) is an
enterprise product feature set that provides the underlying libraries and tools to enable GitLab team members to instrument their code. As the Engineering Manager for the Monitoring team, you will take overall responsibility for
hiring, team management, agile project management, and the quality of the feature set. This position reports to the Director of Engineering, Ops Backend.

See [Engineering Manager](#engineering-manager) for more details.

#### Responsibilities

In addition to the general responsibilities of the Engineering Manager role, the
Monitoring team specifically requires you to be involved in and responsible for:
- Work with the team to define requirements for adding new metrics
- Identifying ways to test and improve availability and performance of GitLab.com Monitoring at scale
- Instrument and monitor the health of distributed GitLab instances through current dashboards
- Educate all team members on best practices relating to monitoring and site reliability

#### Requirements

- Experience with monitoring and alerting of an enterprise SaaS product

#### Hiring Process

The hiring process for this specialty follows the process for most [Engineering
Managers](#engineering-manager).

### Configuration

[GitLab Configuration](/handbook/product/categories/#configure) is a set of features related to GitLab's Application Control Panel, Infrastructure Configuration features, our ChatOps product, Feature flags, and our entire Auto DevOps feature set. As the Engineering Manager for the Configuration team, you will take overall responsibility for hiring, team management, agile project management, and the quality of the feature set. This position reports to the Director of Engineering, Ops Backend.

See [Engineering Manager](#engineering-manager) for more details.

#### Responsibilities

In addition to the general responsibilities of the Engineering Manager role, the
Monitoring team specifically requires you to be involved in and responsible for:
- Work with the team to define requirements for our configuration tools
- Identifying ways to test and improve availability and performance of GitLab.com at scale with different configuration sets
- Instrument and monitor the health of distributed GitLab instances through current dashboards
- Educate all team members on best practices relating to the use of different configuration settings and tools

#### Requirements

- Experience with different deployment configuration
- Experience with function-as-a-service

#### Hiring Process

The hiring process for this specialty follows the process for most [Engineering
Managers](#engineering-manager).

### Gitaly

[Gitaly](https://gitlab.com/gitlab-org/gitaly) is an RPC service for Git
designed to improve the performance of the git data store on large GitLab
instances, particularly GitLab.com. As the Engineering Manager for the Gitaly
team, you will take overall responsibility for hiring, team management, agile
project management, and the quality of this project. This position reports to
the Director of Engineering, Dev Backend.

See [Engineering Manager](#engineering-manager) for more details.

#### Requirements

* Deep experience with Go
* Familiarity with the Git architecture and internals
* Demonstrated success building performant, low-level systems

#### Nice-to-have's

* Familiarity with Git-adjacent technologies, e.g. GVFS

#### Hiring Process

The hiring process for this specialty follows the process for most [Engineering
Managers](#engineering-manager), except for the following:

* The peer interview for any managers considered for the Gitaly team will be
conducted by our Interim Gitaly Lead.
